<?php
define('TAMANO_MATRIZ', 4); // Cambiar el valor según sea necesario

// Función para generar una matriz cuadrada con números aleatorios
function generarMatriz($tamano) {
    $matriz = array();
    for ($i = 0; $i < $tamano; $i++) {
        for ($j = 0; $j < $tamano; $j++) {
            $matriz[$i][$j] = rand(0, 9);
        }
    }
    echo "          ---------            ";
    return $matriz;
}

// Función para imprimir la matriz y calcular la suma de la diagonal principal
function imprimirMatrizYCalcularSuma($matriz) {
    $sumaDiagonal = 0;
    echo "<table border='1'>";
    for ($i = 0; $i < count($matriz); $i++) {
        echo "<tr>";
        for ($j = 0; $j < count($matriz[$i]); $j++) {
            echo "<td>" . $matriz[$i][$j] . "</td>";
            if ($i == $j) {
                $sumaDiagonal += $matriz[$i][$j];
            }
        }
        echo "</tr>";
    }
    echo "</table>";
    return $sumaDiagonal;
}

function ejecutarScript() {
    do {
        $matriz = generarMatriz(TAMANO_MATRIZ);
        $sumaDiagonal = imprimirMatrizYCalcularSuma($matriz);
        if ($sumaDiagonal >= 10 && $sumaDiagonal <= 15) {
            echo "La suma de la diagonal principal es $sumaDiagonal. Fin del script.";
            break;
        }
    } while (true);
}

// Llamada a la función principal
ejecutarScript();
?>
