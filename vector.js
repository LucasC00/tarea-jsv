// Declarar el vector con 6 elementos de diferentes tipos de datos
const vector = [1, 'dos', true, null, { nombre: 'Juan' }, ['elemento1', 'elemento2']];

// Imprimir el vector en la consola
console.log(vector);

// Imprimir el primer y último elemento del vector usando sus índices
console.log('Primer elemento:', vector[0]);
console.log('Último elemento:', vector[vector.length - 1]);

// Modificar el valor del tercer elemento
vector[2] = false;

// Imprimir la longitud del vector
console.log('Longitud del vector:', vector.length);

// Agregar un elemento al final del vector usando "push"
vector.push('nuevo elemento');

// Eliminar el último elemento del vector usando "pop" y guardarlo en una variable
const elementoEliminado = vector.pop();
console.log('Elemento eliminado:', elementoEliminado);

// Agregar un elemento en la mitad del vector usando "splice"
vector.splice(Math.floor(vector.length / 2), 0, 'elemento medio');

// Eliminar el primer elemento del vector usando "shift" y guardarlo en una variable
const primerElemento = vector.shift();
console.log('Primer elemento eliminado:', primerElemento);

// Agregar el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primerElemento);

// Imprimir el vector actualizado en la consola
console.log('Vector actualizado:', vector);