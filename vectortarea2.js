// Declarar el vector con 6 elementos de diferentes tipos de datos
const vector = [1, 'dos', true, null, { nombre: 'Juan' }, ['elemento1', 'elemento2']];

// a. Imprimir en la consola cada valor usando "for"
console.log('Usando for:');
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

// b. Imprimir en la consola cada valor usando "forEach"
console.log('Usando forEach:');
vector.forEach((elemento) => {
  console.log(elemento);
});

// c. Imprimir en la consola cada valor usando "map"
console.log('Usando map:');
vector.map((elemento) => {
  console.log(elemento);
});

// d. Imprimir en la consola cada valor usando "while"
console.log('Usando while:');
let i = 0;
while (i < vector.length) {
  console.log(vector[i]);
  i++;
}

// e. Imprimir en la consola cada valor usando "for..of"
console.log('Usando for..of:');
for (const elemento of vector) {
  console.log(elemento);
}
